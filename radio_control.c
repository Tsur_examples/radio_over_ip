/*
 ============================================================================
 Name        : radio_control.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/select.h>
#include <fcntl.h>
#include <string.h>
/////////////////client to server commands//////////////////
typedef struct Hello{
	uint8_t commandType;
	uint16_t reserved ;
}Hello;
typedef struct AskSong{
	uint8_t commandType;
	uint16_t stationNumber;
}AskSong;

typedef struct UpSong{
uint8_t commandType;
uint32_t songSize; //in bytes
uint8_t songNameSize;
char songName[260];
}UpSong;

////////////////////////server to client commands////////////////
typedef struct Welcome{
uint8_t replyType;//0
uint16_t numStations;
uint32_t multicastGroup;
uint16_t portNumber;
}Welcome;

typedef struct Announce{
uint8_t replyType ;//1
uint8_t songNameSize;
char songName[260];
}Announce;

typedef struct PermitSong{
uint8_t replyType ;//2
uint8_t permit;
}PermitSong;

typedef struct InvalidCommand{
uint8_t replyType ;//3
uint8_t replyStringSize;
char replyString[];//replyStringSize
}InvalidCommand;

typedef struct NewStations{
uint8_t replyType ;//4
uint16_t newStationNumber;
}NewStations;

#define user_input_size 50
#define  tcp_Buffer_Size 1024
#define Numt 2
#define MAX_SONG_SIZE 1024*1024*10
#define MIN_SONG_SIZE 2000
#define UP_DELAY 8000
#define udp_Buffer_Size 1024
#define LISTEN_DELAY 62500

#define waitForWelcome 0
#define established_Connection 1
#define waitForAnnounce 2
#define waitForPermit 3
#define uploadSong 4
#define sometingwong 5
#define quit 6

static void* UDP_connection();
void Send_Hello(int tcpSocket);
void Send_Asksong(int tcpSocket);
void clearbuffer(char* buffer,int buffer_Size);
void waitForWelcomefunc();
void established_connection_func();
void resettimer();
void waitForAnnouncefunc();
void waitForPermitfunc();
void sendupsong();
void uploadSongfunc();
void sometingwongfunc();
void quitfunc();
void PrintProgress( char label[], int step, int total );

pthread_t t;
int tcpSocket , My_UDP_Socket , numOfFds=0 , phase=0 , connected=1,askedToChange=0;
char tcp_Buffer[tcp_Buffer_Size]={0},user_input[user_input_size]={0},udp_Buffer[udp_Buffer_Size]={0};
fd_set fds_set;
struct timeval tv;

FILE * fileP, *udpfp;
Welcome welmsg;
Announce announcemsg;
NewStations stationmsg;
AskSong askmsg;
UpSong upmsg;
PermitSong premitmsg;
InvalidCommand invalmsg;

int main(int argc,char* argv[]) {
//the main will be a speperate thread controling the user input and the tcp messages received from the server
	struct sockaddr_in tcpAddr;

		tcpSocket=socket(AF_INET, SOCK_STREAM, 0);
		if(tcpSocket==-1)
			perror("could not open a tcp socket");


		tcpAddr.sin_family=AF_INET;//family type ipv4
		tcpAddr.sin_addr.s_addr=inet_addr(gethostbyname(argv[1])->h_name);//server ip address/local host
		tcpAddr.sin_port=htons(atoi(argv[2]));//server port

		//connect to the server
		if(connect(tcpSocket,(struct sockaddr*)&tcpAddr,sizeof(tcpAddr))==-1){
			close(tcpSocket);
			perror("2");
			exit(1);
		}
		numOfFds++;
		printf("Connected. Sending Hello message\n");
		while(1)
		{
		switch(phase)
		{
			case waitForWelcome:
				waitForWelcomefunc();
				break;
			case established_Connection:
				established_connection_func();
				break;
			case waitForAnnounce:
				waitForAnnouncefunc();
				break;
			case waitForPermit:
				waitForPermitfunc();
				break;
			case uploadSong:
				uploadSongfunc();
				break;
			case sometingwong:
				sometingwongfunc();
				break;
			case quit:
				quitfunc();
				break;
		}
		}



	return EXIT_SUCCESS;
}

void Send_Hello(int tcpSocket)
{
	Hello msg;
	msg.commandType=0;
	msg.reserved=0;
	tcp_Buffer[0]=htons(msg.commandType);
	tcp_Buffer[1]=htons(msg.reserved);
	if(send(tcpSocket,tcp_Buffer,3,0)<0)
	{
			perror("failed to send Hello message\n");
			exit(1);
	}
}
void Send_Asksong(int tcpSocket)
{
	clearbuffer(tcp_Buffer,1024);
	tcp_Buffer[0]=askmsg.commandType;
	*(uint16_t*)&tcp_Buffer[1]=htons(askmsg.stationNumber);
	if(send(tcpSocket,tcp_Buffer,3,0)<0)
	{
			perror("failed to Send Ask song msg \n");
			exit(1);
	}
	clearbuffer(tcp_Buffer,3);
}

void sendupsong()
{
	tcp_Buffer[0]=upmsg.commandType;
	*(uint32_t* )&tcp_Buffer[1]=htonl(upmsg.songSize);
	tcp_Buffer[5]=upmsg.songNameSize;
	memcpy(tcp_Buffer+6,upmsg.songName,upmsg.songNameSize);
	if(send(tcpSocket,tcp_Buffer,6+upmsg.songNameSize,0)<0)
	{
			perror("failed to Send  UpSong msg \n");
			exit(1);
	}
	clearbuffer(tcp_Buffer,6+upmsg.songNameSize);
}
void clearbuffer(char* buffer,int buffer_Size)
{//clear the given buffer,input buffer pointer,buffer size to clear
	int i;
	for(i=0;i<buffer_Size;i++)
		buffer[i]=0;
	return;
}
void resettimer()
{
//reset the timer and FD SET
	FD_ZERO(&fds_set);
	FD_SET(0, &fds_set);
	FD_SET(tcpSocket, &fds_set);
	tv.tv_sec = 0;
	tv.tv_usec = 1000*100;  		//100 ms
	return;
}

void waitForWelcomefunc(){
	struct in_addr muli;
	int Bytes_Received=0;
	Send_Hello(tcpSocket);
	resettimer();
	select(tcpSocket+1,&fds_set,NULL,NULL,&tv);
	if(FD_ISSET(tcpSocket,&fds_set))
	{
	Bytes_Received = recv(tcpSocket ,tcp_Buffer ,tcp_Buffer_Size,0);
	//printf("%d\n",Bytes_Received);
	if(Bytes_Received==9)
	 {
		if((welmsg.replyType=tcp_Buffer[0])==0)
		{
		welmsg.numStations=ntohs(*(uint16_t*)&tcp_Buffer[1]);
		welmsg.multicastGroup=*(uint32_t*)&tcp_Buffer[3];
		muli.s_addr=ntohl(welmsg.multicastGroup);
		welmsg.portNumber=htons(*(uint16_t*)&tcp_Buffer[7]);
		phase=established_Connection;
		pthread_create(&t,NULL,UDP_connection,NULL);

		printf("num of stations: %d\nmulticast group: %s\nmulticast port:%d\n",welmsg.numStations,inet_ntoa(muli),welmsg.portNumber);
		}
		else
		{
			printf("received wrong reply type from the server to our hello msg!\n");
			phase=quit;
			return;
		}
     }
	else if(Bytes_Received>0)
	{
		printf("welcome message bytes number error\n");
		phase=quit;
		return;
	}
	}//fd_isset_tcp
	else
	{
			printf("timeout has passed before welcome msg \n");
			phase=quit;
	}
	resettimer();
	return;
}//waitforfunc

void established_connection_func(){
	char input[10]={0};
	int inputnum,Bytes_Received=0;
	if(connected==1){
		printf("Please enter a command: \n 1.q to quit \n 2.number from 0 to %d to ask what song is played in the station and switch to that channel \n 3.'s' to upload song\n",(welmsg.numStations-1));//fileno(stdin),fds_set)
		resettimer();
		select(tcpSocket+1,&fds_set,NULL,NULL,NULL);
		if(FD_ISSET(0,&fds_set))//user input
		{
			scanf("%s",input);
			inputnum=atoi(input);
			if(strcmp(input,"q")==0)
			{
				phase=quit;
				return;
			}//quit
			else if(strcmp(input,"s")==0)
			{
				phase=waitForPermit;
			}//upload song

			else if(input[0]=='0' || (inputnum>0 && inputnum<welmsg.numStations))//user request- asksong
			{
				askmsg.commandType=1;
				askmsg.stationNumber=inputnum;
				phase=waitForAnnounce;
				clearbuffer(tcp_Buffer,tcp_Buffer_Size);
			}//inputnum>=0 && inputnum<=welmsg.numStations
			else//wrong input
			{
				printf(" Wrong input\n");
				return;
			}//else wrong input
		}//user_input
		if(FD_ISSET(tcpSocket,&fds_set))
		{
			Bytes_Received = recv(tcpSocket ,tcp_Buffer ,tcp_Buffer_Size,0);
			if(Bytes_Received==3)//received number of bytes that match new stations msg
			{
				stationmsg.replyType=tcp_Buffer[0];
				stationmsg.newStationNumber=ntohs(*(uint16_t*)&tcp_Buffer[1]);

				if(stationmsg.replyType==4)
				{
					(welmsg.numStations)++;
					printf("we received a new station:%d \n",stationmsg.newStationNumber-1);
					return;
				}
				else//error
				{
					printf("received a tcp message that does not match the correct order of sending(waiting for newstations msg)\n ");
					phase=quit;
					return;
				}
			}
			else if(Bytes_Received==0)//server sent close
			{
				printf("server closed the connection\n");
				phase=quit;
				return;
			}
			else
			{
				printf("server sent invalid msg, wrong number of bytes in established state\n");
				phase=quit;
				return;
			}
		}//tcp input
	}//connected
}//establishedfunc

void waitForAnnouncefunc()
{
	int Bytes_Received=0;
	askedToChange=1;
	Send_Asksong(tcpSocket);
	resettimer();
	select(tcpSocket+1,&fds_set,NULL,NULL,&tv);

	if(FD_ISSET(tcpSocket,&fds_set))
	{
		Bytes_Received = recv(tcpSocket ,tcp_Buffer ,tcp_Buffer_Size,0);
	if(Bytes_Received>0)
	{
		announcemsg.replyType=tcp_Buffer[0];
		announcemsg.songNameSize=tcp_Buffer[1];
		strcpy(announcemsg.songName,(char*)&tcp_Buffer[2]);
		if(announcemsg.replyType==1)
		{
			printf("songName: %s\n",announcemsg.songName);
			if(announcemsg.songNameSize==strlen(announcemsg.songName)){
			phase=established_Connection;
			printf("announce msg received!\n");
			}
			else
			{
				printf("Announce msg received wrong song name size/name\n");
				phase=quit;
				return;
			}

		}
		else if(announcemsg.replyType==3)
		{
			printf("Received invalid msg from the server!\n");
			phase=sometingwong;
			return;
		}
		else
		{
			printf("reply type does not match the annouce type\n");
			phase=quit;
			return;
		}
	}
	else if(Bytes_Received==0)//connection closed
	{
		printf("waitForAnnouncefunc: server sent invalid msg, wrong number of bytes in established state\n");
		phase=quit;
		return;
	}
	else//an error accord
	{
		perror("waitforannouce bytes received error");
		phase=quit;
		return;
	}
	clearbuffer(tcp_Buffer,tcp_Buffer_Size);
	resettimer();
	}//FD_ISSET(tcpSocket,&fds_set)
}//waitforannoucefunc

void waitForPermitfunc()
{
	int Bytes_Received=0;
	printf("Please enter the song you would like to upload to the server:\n");
	clearbuffer(upmsg.songName,260);
	scanf("%s",upmsg.songName);
	upmsg.commandType=2;
	upmsg.songNameSize=strlen(upmsg.songName);
	fileP=fopen(upmsg.songName,"r");
	if(fileP==NULL)
	{
		printf("There is no such song, returning to the menu\n");
		phase=established_Connection;
		return;
	}
	fseek(fileP, 0, SEEK_END);
	upmsg.songSize=ftell(fileP);
	rewind(fileP);
	if(upmsg.songSize>=MIN_SONG_SIZE && upmsg.songSize<=MAX_SONG_SIZE)
	{
		resettimer();
		sendupsong();
		if(select(tcpSocket+1,&fds_set,NULL,NULL,&tv)==0)
		{
			printf("waitForPermit:time out passed,quitting...\n");
			phase=quit;
			return;
		}

		else if(FD_ISSET(tcpSocket,&fds_set))
		{
			Bytes_Received = recv(tcpSocket ,tcp_Buffer ,tcp_Buffer_Size,0);
			if(Bytes_Received==2)
			{
				premitmsg.replyType=tcp_Buffer[0];
				premitmsg.permit=tcp_Buffer[1];
				if(premitmsg.replyType==2)
				{
					if(premitmsg.permit==1)
					{
						phase=uploadSong;
						printf("Permit msg received!,start upload song\n");
					}
					else{
						phase=established_Connection;
						printf("the server denied your request,try later!\n");
					}
				}
				else
				{
					printf("reply type does not match the permitSong type\n");
					phase=quit;
				}
			}//Bytes_Received==2
			else if(tcp_Buffer[0]==3)
			{
				phase=sometingwong;
				return;
			}
			else
			{
				printf("waitForPermitfunc: server sent invalid msg, wrong number of bytes\n");
				phase=quit;
			}

		}//FD_ISSET(tcpSocket,&fds_set)
		//fclose(fileP);

	}//upmsg.songSize>=MIN_SONG_SIZE &&
	else
	{
		printf("The song size chosen is out of bounds\n");
		phase=quit;
	}
}//waitForPermitfunc()

void uploadSongfunc()
{
	char c;
	short stationNum;
	useconds_t usec=UP_DELAY;
	int total_sent_bytes=0,sent_bytes_now=0,sel_val,Bytes_Received=0;
	while(total_sent_bytes<upmsg.songSize && EOF!=fscanf(fileP,"%1024c",tcp_Buffer))
	{
		if(connected==0)
		{
			printf("The server shutdown the connection while uploading a song!\n");
			phase=quit;
			return;
		}
		if((upmsg.songSize-total_sent_bytes)>tcp_Buffer_Size)
		sent_bytes_now=send(tcpSocket,tcp_Buffer,tcp_Buffer_Size,0);
		else
			sent_bytes_now=send(tcpSocket,tcp_Buffer,(upmsg.songSize-total_sent_bytes),0);
		if(sent_bytes_now<0)
		{
			printf("Failed to upload a song due to problem sending\n");
			phase=quit;
			return;
		}
		total_sent_bytes+=sent_bytes_now;
		PrintProgress("uploading",total_sent_bytes,upmsg.songSize);

		usleep(usec);
	}//while(bytes...)

	//set the timer to 2 sec according to protocol
	tv.tv_sec=2;
	tv.tv_usec=0;

	sel_val=select(tcpSocket+1,&fds_set,NULL,NULL,&tv);
	if(FD_ISSET(tcpSocket,&fds_set))
	{
		Bytes_Received = recv(tcpSocket ,tcp_Buffer ,tcp_Buffer_Size,0);
					if(Bytes_Received==3)
					{
						stationNum=htons(*(uint16_t*)&tcp_Buffer[1]);
						(welmsg.numStations)++;
						printf("A new station received! station number %d\n",stationNum-1);
						phase=established_Connection;
					}
					else
					{
						printf("Wrong number of bytes received while waiting for new station after uploading song, quitting\n");
						phase=quit;
					}

	}
	else if(sel_val==0)
	{
		printf("timeout occurd while waiting to upload confimration from the server (no new station msg)\n");
		phase=quit;
	}
	resettimer();

}//uploadSongfunc()
void sometingwongfunc()
{
	connected=0;
	invalmsg.replyStringSize=tcp_Buffer[1];
	memcpy(invalmsg.replyString,tcp_Buffer+2,invalmsg.replyStringSize);
	printf("received an invalid msg from the server saying: %s\n",invalmsg.replyString);
	phase=quit;
}
void quitfunc()
{//this function releases all of the resources the program is using
	connected=0;
	pthread_join(t,NULL);
	close(tcpSocket);

	if(My_UDP_Socket)
	close(My_UDP_Socket);

	if(fileP)
	fclose(fileP);

	if(udpfp)
	fclose(udpfp);

	printf("released all of the resources! goodbye\n");
	exit(1);
}


static void* UDP_connection(){
	//the udp connection is responsible for receiving the multicast data sent by the server and playing it.
	int Bytes_Received=0,i=1;
	in_addr_t mulip;
	struct sockaddr_in socketAddress;
	struct ip_mreq mreq;
	socklen_t addrlen;
    struct timeval timeout;
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;


		mulip=welmsg.multicastGroup;
		udpfp = popen("play -t mp3 -> /dev/null 2>&1", "w");
		My_UDP_Socket=socket(AF_INET, SOCK_DGRAM, 0);
		if(My_UDP_Socket==-1)
			perror("socket");

		socketAddress.sin_family = AF_INET;
		socketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
		socketAddress.sin_port = htons(welmsg.portNumber) ;
		mreq.imr_multiaddr.s_addr=htonl(mulip);
		mreq.imr_interface.s_addr=htonl(INADDR_ANY);

		if(bind(My_UDP_Socket,(struct sockaddr *)&socketAddress,sizeof(socketAddress)) == -1)
			{
				perror("could not bind the udp socket\n");
				phase=quit;
				pthread_exit(&t);
			}
			if(setsockopt(My_UDP_Socket,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) == -1)
			{
				perror("error sending add membership request\n");
				phase=quit;
				pthread_exit(&t);
			}
			if(setsockopt(My_UDP_Socket,SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i)))
			{
				perror("could not set the udp option reuseaddr\n");
				phase=quit;
				pthread_exit(&t);
			}
			addrlen=sizeof(socketAddress);
			 if (setsockopt (My_UDP_Socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
			                sizeof(timeout)) < 0)
			 {
			        perror(" udp setsockopt failed\n");
					phase=quit;
					pthread_exit(&t);
			 }
			while(connected==1)
				{
					if(askedToChange==0)
					{
					Bytes_Received = recvfrom(My_UDP_Socket,udp_Buffer ,udp_Buffer_Size, 0 , (struct sockaddr *)&socketAddress, &addrlen);
					if(Bytes_Received<0)
					{
						perror("timeout in the udp connection");
						pthread_exit(&t);
					}
					if(Bytes_Received>0)
					{
						fwrite (udp_Buffer , sizeof(char), udp_Buffer_Size, udpfp);
						Bytes_Received=0;
					}
					else if(Bytes_Received==0)
					{
						printf("connection closed");
						pthread_exit(&t);
					}
					else if (Bytes_Received<0)
					{
						printf("Bytes number error: %d\n ",Bytes_Received);
						pthread_exit(&t);
					}
					}//askedtochange==0
					else
					{
						if(setsockopt(My_UDP_Socket,IPPROTO_IP,IP_DROP_MEMBERSHIP,&mreq,sizeof(mreq)) == -1)
									{
										perror("error sending add membership request\n");
									}
						mulip=askmsg.stationNumber+welmsg.multicastGroup;
						mreq.imr_multiaddr.s_addr=htonl(mulip);
						if(setsockopt(My_UDP_Socket,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) == -1)
									{
										perror("error sending add membership request\n");
									}
						askedToChange=0;
					}
				}
			pthread_exit(&t);
		return EXIT_SUCCESS;
}//UDP_connection

void PrintProgress( char label[], int step, int total )
{
    //progress width
    const int pwidth = 72;

    //minus label len
    int width = pwidth - strlen( label );
    int pos = ( step * width ) / total ;
    int i;

    int percent = ( step * 100 ) / total;

    //set green text color, only on Windows
    printf( "%s[", label );

    //fill progress bar with =
    for ( i = 0; i < pos; i++ )
    	printf( "%c", '=' );

    //fill progress bar with spaces
    printf( "% *c", width - pos + 1, 93 );
    printf( " %3d%%\r", percent );

    //reset text color, only on Windows
    fflush(stdout);
}
