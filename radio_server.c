#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/select.h>
#include <fcntl.h>
#include <string.h>

typedef struct client{
	int active;
	int UID;
	char ip_addr[INET_ADDRSTRLEN];
}client;

typedef struct station{
	int station_num;
	char ip_addr[INET_ADDRSTRLEN];
	client connected_clients[100];
	int numOfClients;
	int songNameSize;
	char songName[260];

}station;

/////////////////client to server commands//////////////////
typedef struct Hello{
	uint8_t commandType;
	uint16_t reserved ;
}Hello;
typedef struct AskSong{
	uint8_t commandType;
	uint16_t stationNumber;
}AskSong;

typedef struct UpSong{
	uint8_t commandType;
	uint32_t songSize; //in bytes
	uint8_t songNameSize;
	char songName[260];
}UpSong;

////////////////////////server to client commands////////////////
typedef struct Welcome{
	uint8_t replyType;//0
	uint16_t numStations;
	uint32_t multicastGroup;
	uint16_t portNumber;
}Welcome;

typedef struct Announce{
	uint8_t replyType ;//1
	uint8_t songNameSize;
	char songName[260];
}Announce;

typedef struct PermitSong{
	uint8_t replyType ;//2
	uint8_t permit;
}PermitSong;

typedef struct InvalidCommand{
	uint8_t replyType ;//3
	uint8_t replyStringSize;
	char replyString[];//replyStringSize
}InvalidCommand;

typedef struct NewStations{
	uint8_t replyType ;//4
	uint16_t newStationNumber;
}NewStations;

#define user_input_size 50
#define  tcp_Buffer_Size 1024
#define Numt 2
#define MAX_SONG_SIZE 1024*1024*10
#define MIN_SONG_SIZE 2000
#define UP_DELAY 8000
#define udp_Buffer_Size 1024
#define LISTEN_DELAY 62500
#define MAX_NUM_OF_CLIENTS 100
#define STARTING_STATION_SIZE 10

#define waitForHelloMsg 0
#define established_Connection 1
#define downloadSong 2
#define waitForPermit 3
#define sometingwong 4
#define invalidcommand 5
#define quit 6




//defines
#define PORTNUMBER 5555
//states defines


//server functions
void resettimer(int UID);
void* statefunc(void *i_void_ptr);
void waitForHello(int UID,client the_client);
void Send_Welcome(int UID);
void send_Annouce(int UID ,int stationNum);
void sendpermit(int UID);
void downloadSongfunc(int UID,client the_client);
void established_connection_func(int UID,client the_client);
void quitfunc();
void clearbuffer(char* buffer,int buffer_Size);
int checkname(char* songName);
void create_stations(int argc,char* argv[100]);
void PrintProgress( char label[], int step, int total );
void sendnewstations(int UID);
void send_invalid(char* error,client the_client);
void sometingwongfunc(client the_client);
void switch_station(client the_client,int to_station);
void* UDP_connection(void *v_station_num);
void printdatabase();
//messages
Hello hellomsg;
Welcome welcomemsg;
UpSong upmsg;


uint16_t NumberOfstation=0;
pthread_t t[MAX_NUM_OF_CLIENTS],*sthread;//todo realloc to add more threads
int tcpSocketArr[MAX_NUM_OF_CLIENTS]={0} , My_UDP_Socket  , phase[MAX_NUM_OF_CLIENTS]={0},serverquit=0, numOfClients=0,stationsize=STARTING_STATION_SIZE;
int downloading=0,udp_port;
char tcp_Buffer_Arr[MAX_NUM_OF_CLIENTS][tcp_Buffer_Size],user_input[user_input_size]={0},udp_Buffer[udp_Buffer_Size]={0};
fd_set fds_set[MAX_NUM_OF_CLIENTS],wel_set;
struct timeval tv[MAX_NUM_OF_CLIENTS];
station *stations;



int main(int argc,char* argv[100]){
	client new_client;
	int Welcome_Socket,i;
	char ip_addr[INET_ADDRSTRLEN];
	struct sockaddr_in wel_socketAddress , Tcp_socket_Add;
	int socketSize;
	stations=(station*)calloc(STARTING_STATION_SIZE,sizeof(station));
	udp_port=htons(atoi(argv[3]));
	sthread=(pthread_t*)calloc(STARTING_STATION_SIZE,sizeof(pthread_t));
	create_stations(argc,argv);

	Welcome_Socket=socket(AF_INET, SOCK_STREAM, 0);
	if(Welcome_Socket==-1)
	{
		perror("could not open welcome socket");
		exit(1);
	}

	wel_socketAddress.sin_family=AF_INET;
	wel_socketAddress.sin_addr.s_addr=htonl(INADDR_ANY);//inet_addr(argv[1])
	wel_socketAddress.sin_port=htons(atoi(argv[1]));


	if(-1 == bind (Welcome_Socket,(struct sockaddr*)&wel_socketAddress,sizeof(wel_socketAddress))){
		perror("could not bind the welcome socket");
		exit(1);
	}
	i=1;
	setsockopt(Welcome_Socket,SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
	printf("welcome socket binding successful %d %d\n",wel_socketAddress.sin_addr.s_addr,wel_socketAddress.sin_port);


	if(-1==listen(Welcome_Socket,MAX_NUM_OF_CLIENTS)){
		perror("could not make welcome socket to listen");
		exit(1);
	}

	printf("Hello! Welcome to the radio project server of Priel and Tsur\n");
	printf("You may enter the following:\n1.'q' to exit the server\n2.'p' to print the stations and user database\n");
	while(1)
	{

		welcomemsg.replyType=0;
		welcomemsg.numStations=NumberOfstation;
		welcomemsg.multicastGroup=inet_addr(argv[2]);
		welcomemsg.portNumber=htons(atoi(argv[3]));


		FD_ZERO(&wel_set);
		FD_SET(0, &wel_set);
		FD_SET(Welcome_Socket, &wel_set);
		select(Welcome_Socket+1,&wel_set,NULL,NULL,NULL);
		//a new client wants to connect to the server
		if(FD_ISSET(Welcome_Socket,&wel_set))
		{
			socketSize=sizeof(Tcp_socket_Add);
			if((tcpSocketArr[numOfClients]=accept(Welcome_Socket,(struct sockaddr*)&Tcp_socket_Add,(socklen_t*)&socketSize))==-1)
			{
				perror("Could not accept a client");
				exit(1);
			}

			inet_ntop( AF_INET, &(Tcp_socket_Add.sin_addr), ip_addr, INET_ADDRSTRLEN );
			clearbuffer(new_client.ip_addr,INET_ADDRSTRLEN);
			for(i=0;i<MAX_NUM_OF_CLIENTS;i++)
			{
				if(stations[0].connected_clients[i].active==0)
				{
					strcpy(new_client.ip_addr,ip_addr);
					new_client.active=1;
					new_client.UID=numOfClients;
					//stations[0].connected_clients[stations[0].numOfClients].active=1;
					stations[0].connected_clients[i]=new_client;
					stations[0].numOfClients++;

					pthread_create( & ( t [numOfClients] ), NULL , statefunc , (void*) &new_client);
					numOfClients++;
					break;
				}
			}
		}//FD_ISSET(Welcome_Socket,&fds_set)

		// we recivd usr input
		if(FD_ISSET(0,&wel_set))
		{
			fgets(user_input,sizeof(user_input),stdin);
			if(user_input[0]=='q' && user_input[1]=='\n')
			{
				printf("user wanted to quit the server,leaving...\n");
				quitfunc();
			}
			else if(user_input[0]=='p' && user_input[1]=='\n')
			{
				printdatabase();
			}
			else
			{
				printf("user typed incorrect input\n");
			}
		}//user input if(FD_ISSET(0,&fds_set))
	}

	return EXIT_SUCCESS;
}


void waitForHello(int UID,client the_client)
{
	int sel_val=0,Bytes_Received=0;
	resettimer(UID);
	sel_val=select((tcpSocketArr[UID])+1,&fds_set[UID],NULL,NULL,&(tv[UID]));

	if(FD_ISSET(tcpSocketArr[UID],&fds_set[UID]))
	{
		Bytes_Received = recv(tcpSocketArr[UID] ,tcp_Buffer_Arr[UID] ,tcp_Buffer_Size,0);
		if(Bytes_Received==3)
		{
			hellomsg.commandType=tcp_Buffer_Arr[UID][0];
			hellomsg.reserved=*(uint16_t*)&tcp_Buffer_Arr[UID][1];

			if(hellomsg.commandType==0 && hellomsg.reserved==0)
			{
				if(numOfClients<100)
				{
					Send_Welcome(UID);
					clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
					printf("Welcome sent to client %s!\n",the_client.ip_addr);
				}
				else
				{
					printf("the server is full , 100 clients are already connected\n");
					phase[UID]=sometingwong;
				}
			}
			else
			{
				printf("the server was expecting hello message from client number %d and instead received a different message\n",UID);
				phase[UID]=sometingwong;

			}
		}

	}//fd_isset_tcp
	else if (sel_val==0)
	{
		printf("timeout has passed while waiting for hello message\n");
		phase[UID]=sometingwong;
	}
	else
	{
		printf("no idea what's wrong while waiting for hello message\n");
		phase[UID]=sometingwong;
	}

}//waitforHello


void Send_Welcome(int UID)
{
	//printf("the multicastGroup number of stations %d starting from :%s ,(NumberOfstation-1), welcomemsg.multicastGroup);"
	tcp_Buffer_Arr[UID][0]=welcomemsg.replyType;
	*(uint16_t* )&tcp_Buffer_Arr[UID][1]=htons(welcomemsg.numStations);
	*(uint32_t* )&tcp_Buffer_Arr[UID][3]=htonl(welcomemsg.multicastGroup);
	*(uint16_t* )&tcp_Buffer_Arr[UID][7]=welcomemsg.portNumber;

	if(send(tcpSocketArr[UID],tcp_Buffer_Arr[UID],9,0)<0)
	{
		perror("failed to send welcome message to client\n");
		phase[UID]=sometingwong;
		return;
	}

	phase[UID]=established_Connection;
}


//server closes the connections to the clients and free all of the resources.
void quitfunc()
{
	int i;
	serverquit=1;
	for(i=0;i<numOfClients;i++)
	{
		pthread_join((t[i]),NULL);
		if(tcpSocketArr[i])
			close(tcpSocketArr[i]);
		//if(tcp_Buffer_Arr[i])
			//free(tcp_Buffer_Arr[i]);
	}
	for(i=0;i<NumberOfstation;i++)
	{
		pthread_exit(&sthread[i]);
		free(&stations[i]);
	}
	printf("freed all of the resources,goodbye\n");
	exit(1);
}
void* statefunc(void *i_void_ptr){
	client the_client=*(client*)i_void_ptr;
	int UID=the_client.UID;
	while(serverquit==0)
	{
		switch(phase[UID])
		{
		case waitForHelloMsg:
			waitForHello(UID,the_client);
			break;
		case established_Connection:
			established_connection_func(UID,the_client);
			break;
		case downloadSong:
			downloadSongfunc(UID,the_client);
			downloading=0;
			break;
		case sometingwong:
			sometingwongfunc(the_client);
			break;
		}
	}
	pthread_exit(&t[UID]);
}

void resettimer(int UID)
{
	FD_ZERO(&fds_set[UID]);
	//FD_SET(0, &fds_set[UID]);
	FD_SET(tcpSocketArr[UID], &fds_set[UID]);
	tv[UID].tv_sec = 0;
	tv[UID].tv_usec = 1000*100;  		//100 ms
	return;
}

void established_connection_func(int UID,client the_client)
{
	short stationNum=0;
	int Bytes_Received=0;
	char songName[260]={0};
	resettimer(UID);

	select((tcpSocketArr[UID])+1,&fds_set[UID],NULL,NULL,&tv[UID]);
	if(FD_ISSET(tcpSocketArr[UID],&fds_set[UID]))
	{
		clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
		Bytes_Received = recv(tcpSocketArr[UID] ,tcp_Buffer_Arr[UID] ,tcp_Buffer_Size,0);
		if(Bytes_Received==3 && tcp_Buffer_Arr[UID][0]==1)
		{
			stationNum=htons(*(short*)&tcp_Buffer_Arr[UID][1]);
			if(stationNum<=NumberOfstation && stationNum>=0)
			{

				printf("received askSong request for station %d from client %s\n",stationNum,the_client.ip_addr);
				send_Annouce(UID,stationNum);
				switch_station(the_client,stationNum);
				clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
			}
			else
			{
				printf("received askSong request with station number %d which is out of bounds\n",stationNum);
				send_invalid("received askSong request with station number which is out of bounds\n",the_client);
				phase[UID]=sometingwong;
				return;

			}
		}
		else if(tcp_Buffer_Arr[UID][0]==2)
		{

			upmsg.songSize=htonl(*(uint32_t*)&tcp_Buffer_Arr[UID][1]);
			if(upmsg.songSize<=MIN_SONG_SIZE || upmsg.songSize>=MAX_SONG_SIZE)
			{
				printf("Song size in the upSong request of client %d is out of bounds\n",UID);
				send_invalid("Song size in the upSong request is out of bounds",the_client);
				//phase[UID]=invalidcommand;
				return;

			}
			strcpy(upmsg.songName,(char*)&tcp_Buffer_Arr[UID][6]);
			if((stationNum=checkname(upmsg.songName))!=-1)//check if the song is not being played atm
			{
				printf("the song %s is already being played in station %d\n",songName,stationNum);
				send_invalid("the song is already being played in a station",the_client);
				//phase[UID]=invalidcommand;
				return;
			}
			upmsg.songNameSize=tcp_Buffer_Arr[UID][5];
			printf("An upsong request was given by client  %s\n for the song %s",the_client.ip_addr,songName);
			sendpermit(UID);
			clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
			phase[UID]=downloadSong;
			return;
		}
		else if(Bytes_Received!=0)
		{
			printf("received a message not in order according to protocol");
			send_invalid("received a message not in order according to protocol",the_client);
			phase[UID]=sometingwong;
			return;
		}
		else
		{
			printf("client %d with ip %s disconnected \n",UID,the_client.ip_addr);
			phase[UID]=sometingwong;
			return;
		}
	}
}
void send_Annouce(int UID ,int stationNum){
	clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
	tcp_Buffer_Arr[UID][0]=1;
	tcp_Buffer_Arr[UID][1]=stations[stationNum].songNameSize;
	memcpy((char*)&tcp_Buffer_Arr[UID][2],stations[stationNum].songName,stations[stationNum].songNameSize);

	if(send(tcpSocketArr[UID],tcp_Buffer_Arr[UID],2+stations[stationNum].songNameSize,0)<0)
	{
		perror("failed to send announce message to client\n");
		phase[UID]=sometingwong;
		return;
	}
	printf("Announce sent to client\n");
	phase[UID]=established_Connection;
}
void clearbuffer(char* buffer,int buffer_Size)
{//clear the given buffer,input buffer pointer,buffer size to clear
	int i;
	for(i=0;i<buffer_Size;i++)
		buffer[i]=0;
	return;
}

int checkname(char* songName)//return the number of station a song is being played at or -1 if
//hes not being played at any station
{
	int i;
	for(i=0;i<NumberOfstation;i++)
	{
		if(strcmp(stations[i].songName,songName)==0)
			return i;
	}
	return -1;
}

void downloadSongfunc(int UID,client the_client)
{
	struct in_addr muli;
	int dataReceived=0,totDataReceived=0;
	FILE* fp;
	if((fp=fopen(upmsg.songName,"w"))==0)
	{
		printf("could not open the file with the file name %s\n",upmsg.songName);
		send_invalid("could not open the file with the file name sent",the_client);
		phase[UID]=sometingwong;
		return;
	}
	while(serverquit!=1)
	{
		dataReceived=0;
		resettimer(UID);
		tv[UID].tv_sec = 3;
		tv[UID].tv_usec = 0;
		if(select((tcpSocketArr[UID])+1,&fds_set[UID],NULL,NULL,&tv[UID])==0)
		{
			printf("timeout has passed while waiting in the download process\n");
			send_invalid("timeout has passed while waiting in the download process",the_client);
			phase[UID]=sometingwong;
			return;
		}
		if(FD_ISSET(tcpSocketArr[UID],&fds_set[UID]))
		{
			if(upmsg.songSize - dataReceived > tcp_Buffer_Size)
				dataReceived=recv(tcpSocketArr[UID] ,tcp_Buffer_Arr[UID] ,tcp_Buffer_Size,0);
			else
				dataReceived=recv(tcpSocketArr[UID] ,tcp_Buffer_Arr[UID] ,upmsg.songSize - dataReceived,0);
			if(dataReceived==0)
			{
				printf("client closed the connection\n");
				send_invalid("client closed the connection",the_client);
				phase[UID]=sometingwong;
				return;
			}

			totDataReceived+=dataReceived;
			fwrite(tcp_Buffer_Arr[UID],1,dataReceived,fp);//fprintf(fp,"%s",tcp_Buffer_Arr[UID]);//write to file
			clearbuffer(tcp_Buffer_Arr[UID],dataReceived);
			PrintProgress("Downloading",totDataReceived,upmsg.songSize);
			if(totDataReceived==upmsg.songSize)
			{
				printf("\ndownloading complete!\n");
				NumberOfstation++;
				if(NumberOfstation>stationsize)
				{
					if((stations=realloc(stations,5))!=0)
					{
						perror("could not realloc for more stations\n");
						phase[UID]=sometingwong;
						return;
					}
					stationsize+=5;
				}
				muli.s_addr=htonl(ntohl(inet_addr(gethostbyname(stations[NumberOfstation-2].ip_addr)->h_name))+1);
				strcpy(stations[NumberOfstation-1].ip_addr,inet_ntoa(muli));
				stations[NumberOfstation-1].station_num=NumberOfstation-1;
				stations[NumberOfstation-1].numOfClients=0;
				stations[NumberOfstation-1].songNameSize=upmsg.songNameSize;
				memcpy(stations[NumberOfstation-1].songName,upmsg.songName,upmsg.songNameSize);
				if(NumberOfstation==stationsize && (sthread=(pthread_t*)realloc(sthread,5))!=0)
				{
					stationsize+=5;
				pthread_create( & ( sthread [NumberOfstation-1] ), NULL , UDP_connection , (void*)&stations[NumberOfstation-1]);
				}
				else
				{
					printf("could not allocate more space for more threads");

				}
				sendnewstations(UID);
				phase[UID]=established_Connection;
				fclose(fp);
				//while ((c = getchar()) != '\n' && c != EOF);
				return;
			}
		}//FD_ISSET(tcpSocketArr[UID],&fds_set
	}//while
}

void create_stations(int argc,char* argv[100]){
	struct in_addr muli;
	int i,j=0;

	NumberOfstation=argc-4;
	if(NumberOfstation>stationsize)
		if((stations=realloc(stations,NumberOfstation-stationsize))==0)
		{
			stationsize+=NumberOfstation-stationsize;
			perror("could not realloc for more stations\n");
			quitfunc();
		}
	for (i=4;i<argc;i++,j++)
	{
		stations[j].station_num=j;
		stations[j].numOfClients=0;
		stations[j].songNameSize=strlen(argv[i]);
		memcpy(stations[j].songName,argv[i],stations[j].songNameSize);
		muli.s_addr=htonl(ntohl(inet_addr(gethostbyname(argv[2])->h_name))+j);
		strcpy(stations[j].ip_addr,inet_ntoa(muli));
		pthread_create( & ( sthread [j] ), NULL , UDP_connection , (void*)&stations[j]);
	}
}

void sendpermit(int UID){
	//send permit=1 if the server doesnt download currently else permit=0
	tcp_Buffer_Arr[UID][0]=2;
	if(downloading==0)
	{
		tcp_Buffer_Arr[UID][1]=1;
		downloading=1;
	}
	else
		tcp_Buffer_Arr[UID][1]=0;
	if(send(tcpSocketArr[UID],tcp_Buffer_Arr[UID],2,0)<0)
	{
		perror("failed to send permit message to client\n");
		phase[UID]=sometingwong;
		return;
	}
}


void PrintProgress( char label[], int step, int total )
{
	//progress width
	const int pwidth = 72;

	//minus label len
	int width = pwidth - strlen( label );
	int pos = ( step * width ) / total ;
	int i;

	int percent = ( step * 100 ) / total;

	//set green text color, only on Windows
	printf( "%s[", label );

	//fill progress bar with =
	for ( i = 0; i < pos; i++ )
		printf( "%c", '=' );

	//fill progress bar with spaces
	printf( "% *c", width - pos + 1, ']' );
	printf( " %3d%%\r", percent );

	//reset text color, only on Windows
	fflush(stdout);
}

void sendnewstations(UID){
	int i;
	tcp_Buffer_Arr[UID][0]=4;
	*(uint16_t*)&tcp_Buffer_Arr[UID][1]=htons(NumberOfstation);
	for(i=0;i<numOfClients;i++)
	{
		if(send(tcpSocketArr[i],tcp_Buffer_Arr[UID],3,0)<0)
		{
			perror("failed to send new stations message to client\n");
			phase[UID]=sometingwong;
			return;
		}
	}
}

void send_invalid(char* error,client the_client)
{
	int UID=the_client.UID,len=strlen(error);
	clearbuffer(tcp_Buffer_Arr[UID],tcp_Buffer_Size);
	tcp_Buffer_Arr[UID][0]=3;
	tcp_Buffer_Arr[UID][1]=len;
	strcpy((char*)&tcp_Buffer_Arr[UID][2],error);
	if(send(tcpSocketArr[UID],tcp_Buffer_Arr[UID],len+3,0)<0)
	{
		printf("failed to send new stations message to client %s \n",the_client.ip_addr);
		phase[UID]=sometingwong;
	}
}

void sometingwongfunc(client the_client){
	int UID=the_client.UID;
	printf("client %s was thrown from the server\n",the_client.ip_addr);
	switch_station(the_client,-1);//remove the client from the station
	phase[UID]=waitForHelloMsg;
	numOfClients--;
	close(tcpSocketArr[UID]);
	pthread_exit(&t[UID]);
}

void switch_station(client the_client,int to_station){
	int i,j,k;
	for(i=0;i<NumberOfstation;i++)
	{

		for(j=0;j<stations[i].numOfClients;j++)
		{
			if(stations[i].connected_clients[j].active==1 && strcmp(stations[i].connected_clients[j].ip_addr,the_client.ip_addr)==0)
			{
				stations[i].numOfClients--;
				if(to_station==-1)
				{
					stations[i].connected_clients[j].active=0;
					return;
				}
				else
				{
					for(k=0;k<MAX_NUM_OF_CLIENTS;k++)
					{
						if(stations[to_station].connected_clients[k].active==0)
						{
							stations[to_station].connected_clients[k].active=1;
							stations[to_station].numOfClients++;
							stations[to_station].connected_clients[j]=stations[i].connected_clients[j];
							stations[i].connected_clients[j].active=0;
							return;
						}
					}
				}
			}
		}
	}
	printf("could not find the client in the stations list\n");

}
void* UDP_connection(void *v_station){
	station this_station=*(station*)v_station;
	FILE * file_p=fopen(this_station.songName,"r");
	int TTL=10,My_UDP_Socket=socket(AF_INET,SOCK_DGRAM,0);
	char Buffer[udp_Buffer_Size];
	struct sockaddr_in socketAddress;

	if(file_p==0)
	{
		printf("could not open the file with the file name %s\n written in the input",this_station.songName);
		fclose(file_p);
		close(My_UDP_Socket);
		serverquit=1;
		pthread_exit(&sthread[this_station.station_num]);
	}
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_addr.s_addr = inet_addr(this_station.ip_addr);
	socketAddress.sin_port = udp_port;

	if(bind(My_UDP_Socket,(struct sockaddr *)&socketAddress,sizeof(socketAddress)) == -1)
	{
		perror("bind");
		serverquit=1;
		fclose(file_p);
		close(My_UDP_Socket);
		pthread_exit(&sthread[this_station.station_num]);
	}
	if(setsockopt(My_UDP_Socket, IPPROTO_IP, IP_MULTICAST_TTL, &TTL, sizeof(TTL))==-1) //set TTL field to 10
	{
		perror("setsockopt:");

	}
	while(1)
	{
		while(serverquit!=1 && EOF!=fscanf(file_p,"%1024c",Buffer))
		{
			if(sendto(My_UDP_Socket,Buffer,sizeof(Buffer),0,(struct sockaddr *)&socketAddress,sizeof(socketAddress))==-1)
			{
				perror("sendto");
				fclose(file_p);
				close(My_UDP_Socket);
				exit(1);

			}
			usleep(62500);
		}
		if(serverquit==1)
		{
			fclose(file_p);
			pthread_exit(&sthread[this_station.station_num]);
		}
		else
			rewind(file_p);
	}//while(1)
	fclose(file_p);
	close(My_UDP_Socket);
}

void printdatabase(){
	int i,j;
	for (i=0;i<NumberOfstation;i++)
	{
		printf("--------- station number %d --------------\n\n",i);
		printf("song: %s\n",stations[i].songName);
		printf("number of clients: %d\n",stations[i].numOfClients);
		for(j=0;j<MAX_NUM_OF_CLIENTS;j++)
		{
			if(stations[i].connected_clients[j].active==1)
			{
			printf("client number: %d\n",j);
			printf("IP address: %s\n\n",stations[i].connected_clients[j].ip_addr);
			}
		}
		printf("\n");
	}
}
